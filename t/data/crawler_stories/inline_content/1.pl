#<<<
$VAR1 = {
          'collect_date' => '2014-06-16 13:27:21',
          'content' => '<p>This is the first item.</p>
',
          'db_row_last_updated' => '2014-06-16 16:27:24.678252+03',
          'description' => 'RSS description of the first inline item.',
          'extracted_text' => 'First inline item
***

RSS description of the first inline item.
***

',
          'feed_type' => 'syndicated',
          'full_text_rss' => 0,
          'guid' => 'http://www.example.com/inline_content/item_1.html',
          'language' => 'en',
          'media_id' => 1,
          'publish_date' => '2009-06-11 10:57:00',
          'stories_id' => 1,
          'story_sentences' => [
                                 {
                                   'db_row_last_updated' => '2014-06-16 16:27:24.678252+03',
                                   'language' => 'en',
                                   'media_id' => 1,
                                   'publish_date' => '2009-06-11 10:57:00',
                                   'sentence' => 'First inline item.',
                                   'sentence_number' => 0,
                                   'stories_id' => 1,
                                   'story_sentences_id' => '1'
                                 },
                                 {
                                   'db_row_last_updated' => '2014-06-16 16:27:24.678252+03',
                                   'language' => 'en',
                                   'media_id' => 1,
                                   'publish_date' => '2009-06-11 10:57:00',
                                   'sentence' => 'RSS description of the first inline item..',
                                   'sentence_number' => 1,
                                   'stories_id' => 1,
                                   'story_sentences_id' => '2'
                                 }
                               ],
          'tags' => [],
          'title' => 'First inline item',
          'url' => 'http://tundra.local:51690/inline_content/item_1.html'
        };
#>>>
